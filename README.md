*About First Data Payment Integration*

> [*Use Cases*](#use-cases)
>
> [*Limitations, Constraints*](#limitations-constraints)
>
> [*Compatibility*](#compatibility)
>
> [*Privacy*](#privacy)
>
> [*Integration Overview*](#integration-overview)
>
> [*Items Provided for Integration*](#h.cstjp7omo05a)
>
> [*Integration Steps*](#h.hiom1y34olmx)
>
> [*Step 1: Installing the Cartridge*](#modified-storefront-javascript)
>
> [Step 2: Site Preferences](#step-2-site-preferences)
>
> *Step 3: CO-Billing modifications*
>
> [*Testing*](#testing)
>
> [*Release History*](#release-history)

This document outlines the installation instructions for the cartridge
that handles FirstData (Payeezy) payment Demandware integration. The
cartridge was developed by LyonsCG. Lyons CG owns all proprietary rights
to the source code and documentation of this integration.

About First Data (Payeezy) Integration
======================================

This integration implements PreAuthorization of the credit card
transactions, authorization of gift cards payments, tokenizing the
credit cards and OCAPI integration.

Use Cases
=========

This cartridge can be installed for projects any combination of
requirements: CC pre-authorzation, gift card redemption, tokenization of
credit card on billing and account cards intercept, OCAPI integration.

Limitations, Constraints
========================

-   The calls to the Payeezy (FirstData) APIs are not developed using
    DWRE framework;

-   The cartridge is not written for use pipeline implementation and is
    not customized for controllers;

-   PayPal payment method is not implemented by this cartridge (Future
    updates release may include PayPal First Data solutions harvested
    from Lyons projects).

Compatibility
=============

The cartridge has been developed on and tested for Demandware version
15.9.

Privacy
=======

This cartridge deals with PCI compliancy concerns of storing credit card
data in Demandware. With full disclosure of Demandware PCI
certification, few clients choose to raise the level of PCI compliance
by employing tokenization libraries available through the Payment
Gateway providers. In case of First Data (Payeezy) payment gateway
integration, a JS tokenization library is available and built into this
cartridge.

Integration Overview
====================

Integration Files
=================

Int\_firstdata cartridge contains First Data integration specific forms,
pipelines, DS scripts, ISML templates and Javascript files. All API
calls are implemented through REST services in JSON format. There is
also a non-OCAPI integration cartridge sample provided. TOREVIEW

*Pipelines*:

FirstData.xml – int\_firstdata/cartridge/pipelines/FirstData.xml

The pipeline handles the CC PreAuth call, Gif Certificate balance check
and redemption, and a credit card authorization void transaction API
calls.

FIRSTDATA\_CREDIT.xml

app\_storefront/cartridge/pipelines/FIRSTDATA\_CREDIT.xml

This is the payment instrument hook pipeline that gets called
dynamically by for the “FIRSTDATA\_CREDIT” payment instrument defined in
the Business Manager, it acts as a hook for First Data credit card calls
in the Fistdata.xml pipeline.

FIRSTDATA\_GIFTCERTIFICATE.xml-

app\_storefront/cartridge/pipelines/FIRSTDATA\_GIFTCERTIFICATE.xml

This is the payment instrument hook pipeline that gets called
dynamically by for the “FIRSTDATA\_GIFTCERTIFICATE” payment instrument
defined in the Business Manager, it acts as a hook for First Data gift
certificate calls in the Fistdata.xml pipeline.

*Modified Storefront Pipelines*:

COBilling.xml-

c/cartridge/pipelines/COBilling.xml

The pipeline is modified to skip credit card validation if the card has
been tokenized.

PaymentInstruments.xml -

app\_storefront/cartridge/pipelines/PaymentInstruments.xml

The payment instruments pipeline is modified to handle the tokenization
and storing the token in place of the credit card number. TOREVIEW

####### Scripts:

CreditCardOperationsGGe4.ds -
int\_firstdata/cartridge/scripts/creditcard/CreditCardOperationsGGe4.ds

This script handles all credit card operations with First Data API.

CreditPreAuthGGe4.ds -

int\_firstdata/cartridge/scripts/creditcard/CreditPreAuthGGe4.ds

This script handle the Pre-Authorization call to First Data API.

CreditVoidAuthGGe4.ds -

int\_firstdata/cartridge/scripts/creditcard/CreditVoidAuthGGe4.ds

This script handle the Void call to First Data API.

CheckRemainingOrderTotal.ds -

int\_firstdata/cartridge/scripts/giftcard/CheckRemainingOrderTotal.ds

The script checks the order total against the applied gift certificates.

CreateGiftCertificatePaymentInstrument.ds -

int\_firstdata/cartridge/scripts/giftcard/CreateGiftCertificatePaymentInstrument.ds

The script creates the gift certificate payment instrument.

GetGiftCertBalanceGGe4.ds -

int\_firstdata/cartridge/scripts/giftcard/GetGiftCertBalanceGGe4.ds

This script handle the gift card balance check call to First Data API.

GiftCardOperationsGGe4.ds -

int\_firstdata/cartridge/scripts/giftcard/GiftCardOperationsGGe4.ds

This script handles all gift card operations with First Data API.

GiftCardVoidGGe4.ds -

int\_firstdata/cartridge/scripts/giftcard/GiftCardVoidGGe4.ds

This script handle the gift card void call to First Data API.

RedeemGiftCertGGe4.ds -

int\_firstdata/cartridge/scripts/giftcard/RedeemGiftCertGGe4.ds

This script handle the gift card redemption call to First Data API.

FirstDataUtils.ds -

int\_firstdata/cartridge/scripts/util/FirstDataUtils.ds

The script performs utility functions for First Data request parameter
mapping and hash calculation.

HashUtils.ds -

int\_firstdata/cartridge/scripts/util/HashUtils.ds

The script handle the hash code calculation based on the HMAC key for
API access authentication.

RemoveCCPaymentInstrument.ds

int\_firstdata/cartridge/scripts/util/RemoveCCPaymentInstrument.ds

The script removes First Data credit card payment instrument from the
basket.

*Modified Storefront Pipelines*:

DuplicateCheck.ds

app\_core/cartridge/scripts/account/payment/DuplicateCheck.ds

The script is modified to check the tokenized cards for duplicates by
the BIN and last four digits of the token.

####### JavaScript:

payeezy.js -

int\_firstdata/cartridge/static/default/payeezy/js/payeezy.js

This is the tokenization JavaScript intercept used on the credit card
form. It is provided by the First Data (Payeezy technical
representative), but contains some changes and additions by Lyons
Consulting Group. The use of the tokenization code is optional and will
pertain to the payment integration requirement of a given project/
client.

payeezyresponse.js -

int\_firstdata/cartridge/static/default/payeezy/js/payeezyresponse.js

This JavaScript code handles the Payeezy tokenization response and
captures the return multi-use card token.

####### Modified Storefront JavaScript:

app\_storefront\_richUI/cartridge/js/pages/checkout/billing.js

####### ISML templates:

firstdatapayeezyaccount.isml -

int\_firstdata/cartridge/templates/default/creditcard/firstdatapayeezyaccount.isml

This template copies the billing card form inputs on the account card
add page into the Payeezy form fields for the subsequent form post to
Payeezy tokenization endpoint. This allows for saving a credit card on
DW account profile with a token and not the credit card number.

firstdatapayeezybilling.isml

int\_firstdata/cartridge/templates/default/creditcard/firstdatapayeezybilling.isml

This template copies the billing card form inputs on the billing page
into the Payeezy form fields for the subsequent form post to Payeezy
tokenization endpoint. This allows for submitting an authorization call
to the First Data (Payeezy) payment gateway with a stored token instead
of the credit card number.

firstdatapayeezyinit.isml -

int\_firstdata/cartridge/templates/default/creditcard/firstdatapayeezyinit.isml

This template initiates the call to the Payeezy tokenization API with
all configured site preference values for the endpoint and
authentication keys.

firstdatapayeezyinputs.isml

int\_firstdata/cartridge/templates/default/creditcard/firstdatapayeezyinputs.isml

This template initializes Payeezy form inputs. They stay hidden on the
form and must not have name attributes.

errors\_handler.isml -

int\_firstdata/cartridge/templates/default/gcapi/errors\_handler.isml

The template handles JSON response on GC application errors.

order\_success\_json.isml -

int\_firstdata/cartridge/templates/default/gcapi/order\_success\_json.isml

The template handles JSON response on order success.

success\_json.isml -

int\_firstdata/cartridge/templates/default/gcapi/success\_json.isml

The template handles JSON response on GC application success.

####### Metadata XML:

int\_firstdata/FirstDataSystemObjectsExtensions.xml

This is a metadata file for upload and import into the Business Manager.
TOREVIEW

Step 1: Installing the Cartridge
--------------------------------

1.  Add the cartridge into your Eclipse work space.

2.  Upload and link the cartridge to the server

3.  Add the cartridge to the cartridge path of the appropriate web site

Step 2: Site Preferences
------------------------

1.  Import Site Preferences object extensions

2.  If using LCG gulp builder, add the cartridge to the configuration

> ![](media/image1.png){width="4.304166666666666in"
> height="3.904166666666667in"}

1.  Steps for GC

> app\_storefront\_richUI/cartridge/js/pages/checkout/billing.js
>
> Modify the billing.js as shown in billing\_EXAMPLE.js.
>
> Add this code right after line 72 in the exports.init = function ().
>
> var \$giftCertName = \$('input\[name\$="\_giftCertName"\]');
>
> var \$giftCertPIN = \$('input\[name\$="\_giftCertPIN"\]');

Add this code after line 125 in the \$addGiftCert.on('click', function
(e)

var name = \$giftCertName.val();

var pin = \$giftCertPIN.val();

Add these lines of code right after line in 32

app\_storefront/cartridge/templates/resources/checkout.properties

billing.giftcertnamemissing=Please enter the name on the gift
certificate.

billing.giftcertpinmissing=Please enter the PIN on the gift certificate.

billing.giftcertidmissing=Please enter a gift certificate code.

Add these lines of code right after line 169 in the billing.isml as
shown in the

billing\_EXAMPLE.isml

&lt;isinputfield formfield="\${pdict.CurrentForms.billing.giftCertName}"
type="input"

rowclass="label-above"/&gt;

&lt;isinputfield formfield="\${pdict.CurrentForms.billing.giftCertPIN}"
type="input"

rowclass="label-above"/&gt;

Step 2: Custom Code
-------------------

Modify COBilling – Start pipeline, replace the
"COBilling-RedeemGiftCertificate” call node

with “FirstData-RedeemGiftCertificate” call node.

![](media/image2.png){width="4.0in" height="5.3in"}

Modifi COBilling-ValidateBilling pipeline node to skip credit card
validation if the card has been tokenized.

![](media/image3.png){width="4.864724409448819in"
height="4.09565179352581in"}

Modify COBilling-CleanPaymentInstruments pipeline node

![](media/image4.png){width="4.330555555555556in"
height="4.738888888888889in"}

Modify COBilling-UpdateSummary pipeline node.

![](media/image5.png){width="3.5652777777777778in"
height="4.278472222222222in"}

![](media/image6.png){width="6.495833333333334in"
height="2.2694444444444444in"}

Modify COBilling-AdjustGiftCertificates pipeline node.

![](media/image7.png){width="3.686956474190726in"
height="4.1011067366579175in"}

![](media/image8.png){width="6.495833333333334in"
height="2.6694444444444443in"}

Modify PaymentInstruments-Add pipeline node to skip credit card
validation if token is present, to save the token on the Payment
Instrument in the credit card number and creditCardToken fields.

![](media/image9.png){width="3.9718613298337706in"
height="4.843478783902012in"}

![](media/image10.png){width="4.094776902887139in"
height="4.827657480314961in"}

![](media/image11.png){width="3.5739129483814525in"
height="4.467391732283464in"}

Modify the paymentinstrumentdetails.isml to include the First Data
templates and card token field.

![](media/image12.png){width="6.495833333333334in"
height="3.738888888888889in"}

Modify the creditcardjson.isml to include the card token.

![](media/image13.png){width="6.373611111111111in"
height="3.338888888888889in"}

Modify the paymentmethods.isml to include the First Data templates and
the card token hidden field.

![](media/image14.png){width="6.5in" height="2.6in"}

Modify billing.js to cache the FirstData includes (ln.:11), set form
credit card number and token fields (ln.:24-28, ln.:38-42), and populate
the credit card form (ln.:

![](media/image15.png){width="6.495833333333334in"
height="2.5652777777777778in"}

![](media/image16.png){width="6.495833333333334in"
height="3.990972222222222in"}

![](media/image17.png){width="6.4in" height="4.2in"}

Modify appresources.isml to add the log pipeline to the “urls”
declarations as shown in appresources\_EXAMPL.isml.

![](media/image18.png){width="6.495833333333334in"
height="3.9131944444444446in"}

<span id="h.7tqqer6tsbk9" class="anchor"><span id="_Step_3:_Basket" class="anchor"><span id="_Step_4:_Cart" class="anchor"></span></span></span>
------------------------------------------------------------------------------------------------------------------------------------------------

Testing<span id="h.mlj43h57vuej" class="anchor"><span id="h.sev0b6h42eg1" class="anchor"><span id="h.wl1xvu8abb0e" class="anchor"></span></span></span>
=======================================================================================================================================================

Testing the cartridge requires obtaining a test account with First Data
will all necessary configurations for the features implemented by the
cartridge.

Release History
===============

  09/23/2015   Oksana Nazhmetdinova   Created
  ------------ ---------------------- ---------