/*  order_hook_scripts.js  */

importPackage( dw.system );
importPackage( dw.io );
importPackage( dw.util );
importPackage( dw.order );
importPackage( dw.util );
importPackage( dw.order.hooks);

importScript( "int_firstdata:creditcard/CreditCardOperationsGGe4.ds");
importScript( "int_firstdata:giftcard/GiftCardOperationsGGe4.ds");
importScript( "app_core:ocapi/OrderPaymentUtils.ds");

importScript( "int_accertify:export/createOrderRequest.ds");
importScript( "int_accertify:/paymentGateway/utilityFunctions.ds" );
importScript( "int_accertify:/export/order.ds" );
importScript( "int_alliancedata:torrid/TorridCardSale.ds" );
importScript( "int_smartbutton:rewards/SmartButtonRewards.ds" );
importScript("app_core:cart/getproratedprices.ds");
importScript("int_e_dialog:placeOrder.ds");

exports.beforePaymentAuthorize = function(order, paymentInstrument)
{
	dw.system.Logger.info("BEFORE PAYMENT AUTHORIZE HOOK: START");
	var requestClientID = request.httpHeaders.get("x-dw-client-id");
	var ocapiClient : Boolean = (requestClientID != null);
	var returnRecommendation : String= "";
	var remarks : String = "";
	var total_score : String = "";
	var cross_reference : String= "";
	var rules_tripped : String = "";
	var transactionID : String = "";
	
	if (dw.system.Site.getCurrent().getCustomPreferenceValue('accertifyEnabled')) {
		var AccertifyResponse = performOrderFraudCheck(order, session, ocapiClient);
	} else {
		var AccertifyResponse = false;
		dw.system.Logger.info('ACCERTIFY DISABLED! FRAUD CHECK SKIPPED!');
	}
	
	if (paymentInstrument != null) {
		if (!AccertifyResponse) {
			order.custom.fraudStatus = "NULL";
			order.custom.fraudMessage = "No fraud check has taken place";
		} else {
			for each ( var element : XML in AccertifyResponse.* ) {
				if ( element.nodeKind() == "element" ) {
					if (element.localName()=="recommendation-code") {
						returnRecommendation = element.toString().toLowerCase();
					} else if (element.localName()=="remarks") {
						remarks = element.toString().toLowerCase();
					} else if (element.localName()=="transaction-id") {
						transactionID = element.toString().toLowerCase();
					} else if (element.localName()=="cross-reference") {
						cross_reference = element.toString().toLowerCase();
					} else if (element.localName()=="total-score") {
						total_score = element.toString();
					} else if (element.localName()=="rules-tripped") {
						rules_tripped = element.toString();
					}else if (element.localName()=="transaction-id") {
						transactionID = element.toString();
					}
				}
			}
			order.custom.fraudStatus = returnRecommendation;
			order.custom.fraudMessage = remarks;
			order.custom.recommendationCode = returnRecommendation;
			order.custom.remarks = remarks;
			order.custom.AccertifyTransactionID = transactionID;
			order.custom.crossReference = cross_reference;
			order.custom.totalScore = total_score;
			order.custom.rulesTripped = rules_tripped;
		}
		
	}
	
	dw.system.Logger.info("BEFORE PAYMENT AUTHORIZE HOOK: END");
	return new Status(Status.OK, Status.OK, 'OK', AccertifyResponse);
}
// TODO: re-factor into an internal authorize hook call inside of afterPaymentAuthorize hook
exports.afterPaymentAuthorize = function(order : Order, paymentInstrument : OrderPaymentInstrument, remoteAddress : String, ccSecurityCode : String)
{
	
	dw.system.Logger.info("AFTER PAYMENT AUTHORIZE HOOK: START");
	var status : Status = new Status(Status.ERROR);
	var orderStatus : Status = new Status(Status.ERROR);
	// for re-usability purposes, set the flag determining the origin of the call (storefront or OCAPI client)
	var requestClientID = request.httpHeaders.get("x-dw-client-id");
	var ocapiClient : Boolean = (requestClientID != null);
	try
	{
		// pull the payment method from the payment instrument or the OCAPI JSON request document 
		var paymentMethod : String = (ocapiClient) ? paymentInstrument.payment_method_id.split('.')[0] : paymentInstrument.getPaymentMethod();
		// take the incoming payment instrument of the storefront call or build one from the OCAPI request
		var pi : OrderPaymentInstrument = (ocapiClient) ? OCAPIPaymentUtils.buildOcapiPaymentInstrument(order, paymentInstrument) : paymentInstrument;
		
		var authorization : Object = null;
		
		switch(paymentMethod)
		{
			case "CREDIT_CARD" :
				dw.system.Logger.info("AFTER PAYMENT AUTHORIZE HOOK: CREDIT CARD USED");
				// call the authorization library
				if(pi.creditCardType == 'Brand') {
					authorization = TorridCardSale.preAuthorize(order, pi, remoteAddress);
				} else {
					if(ocapiClient) {ccSecurityCode = paymentInstrument.payment_card.security_code;}
					authorization = CreditCardOperations.preAuthorize(order, pi, remoteAddress, ccSecurityCode);
				}
				// handle failures and errors
				if(authorization == null){
					dw.system.Logger.info("AFTER PAYMENT AUTHORIZE HOOK: Authorization = null");
					status = new Status(Status.ERROR);
					break;
				}
				if(!authorization.approved) {
					dw.system.Logger.info("AFTER PAYMENT AUTHORIZE HOOK: Authorization != approved. " );
					status = new Status(Status.ERROR, Status.ERROR, "Failed Authorization",authorization);
					break;
				}								
				// approved transaction status returns the authorization object
				dw.system.Logger.info("AFTER PAYMENT AUTHORIZE HOOK: Authorization = SUCCESS");
				status = new Status(Status.OK, Status.OK, "Approved",authorization);
				break;
			
			case "GIFT_CERTIFICATE" :
				dw.system.Logger.info("AFTER PAYMENT AUTHORIZE HOOK: GC USED");
				authorization = GiftCardOperations.redeemAmount(order, pi);
				if( authorization == null || !empty(authorization.error)){
					status = new Status(Status.ERROR, GiftCertificateStatusCodes.GIFTCERTIFICATE_NOT_FOUND );
					break;
				}
				status = new Status(Status.OK, Status.OK, "Redeemed", authorization);
				break;	
			
			default :
				dw.system.Logger.info("AFTER PAYMENT AUTHORIZE HOOK: Payment method {0} not implemented", paymentMethod);
		}
		
		// set confirmation and export statuses for an OCAPI store order
		if(ocapiClient) {
			if(status.status == Status.OK) {
				if(OCAPIPaymentUtils.orderAuthorized(order)) {
					orderStatus = OrderMgr.placeOrder(order);
					order.setConfirmationStatus(Order.CONFIRMATION_STATUS_CONFIRMED);
					order.setExportStatus(Order.EXPORT_STATUS_READY);
					order.custom.paymentStatus = "OK";
				} 
				else{
					if(PaymentInstrument.METHOD_GIFT_CERTIFICATE.equals(paymentMethod)){
						orderStatus = new Status(Status.OK);
						order.custom.paymentStatus = "OK";
					}
				}

				var totalAuthAmount : Number = 0;
				for each (opi in order.paymentInstruments) {
					if (opi.paymentTransaction.custom.status == 'OK') {
						totalAuthAmount = totalAuthAmount + opi.paymentTransaction.amount.value;
					}
				}
				
				if (Math.round(totalAuthAmount*100)/100 == order.totalGrossPrice.value) {
					//send E-dialog confirmation email.
					if("shipToStoreNumber" in order.shipments[0].shippingAddress.custom && order.shipments[0].shippingAddress.custom.shipToStoreNumber){
						ConfirmationEmail.Send("store", order);
					} else {
						ConfirmationEmail.Send("simple", order);
					}
				}
				
			} else {
				orderStatus = OrderMgr.failOrder(order);
				order.custom.paymentStatus = "ERROR";
				order.custom.paymentMessage = authorization.error;
			}
		} else {
			if(status.status == Status.OK) {
				orderStatus = new Status(Status.OK);
				order.custom.paymentStatus = "OK";
				// Redeem SmartButton Rewards
				var rewardStatus = SmartButtonRewards.redeemRewards(order);
				if (rewardStatus == "OK") {
					SmartButtonRewards.updateRewards(session);
				}
				order.custom.paymentMessage = "OK";
			} else {
				if(!authorization.paymentGatewayActive){
					orderStatus = new Status(Status.OK, Status.OK, "Payment Processor inactive or Payment Gateway inaccessible", authorization);
					order.custom.paymentStatus = "RETRY";
					order.custom.paymentMessage = authorization.error;
				}
				else{
					orderStatus = new Status(Status.ERROR, Status.ERROR, "Failed Authorization", authorization);
					order.custom.paymentStatus = "DECLINED";
					order.custom.paymentMessage = authorization.error;
					for each (var paymentIns in order.paymentInstruments) {
						if(paymentIns.paymentMethod == paymentIns.METHOD_GIFT_CERTIFICATE) {
							GiftCardOperations.voidRedemption(order.orderNo, paymentIns);
						}
					}
				}
			}
		}
	}
	catch(e)
	{
		var error = e;//debugging
		Logger.error("After Payment Authorize Error: "+e);
	}
	return orderStatus;
}

