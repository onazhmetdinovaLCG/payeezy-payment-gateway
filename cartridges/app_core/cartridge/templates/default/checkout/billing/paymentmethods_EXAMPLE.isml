 <iscontent type="text/html" charset="UTF-8" compact="true"/>
<!--- TEMPLATENAME: paymentmethods.isml --->
<isinclude template="util/modules"/>

<iscomment>get gift cards applied total</iscomment>
<isset name="gcPITotal" value="${0}" scope="pdict"/>
<isif condition="${pdict.Basket.giftCertificatePaymentInstruments.size() > 0}">
	<isloop items="${pdict.Basket.giftCertificatePaymentInstruments}" var="giftCertPI">
		<isset name="gcPITotal" value="${pdict.gcPITotal + giftCertPI.paymentTransaction.amount}" scope="pdict"/>
	</isloop>
</isif>
<isset name="OrderTotal" value="${pdict.Basket.totalGrossPrice.value}" scope="pdict"/>

<h2>${Resource.msg('minibillinginfo.paymentmethod','checkout',null)}</h2>

<isif condition="${pdict.OrderTotal > 0 && pdict.gcPITotal < pdict.OrderTotal}">
	<fieldset>

		<legend>
			${Resource.msg('billing.paymentheader','checkout',null)}
			<div class="dialog-required"> <span class="required-indicator">&#42; <em>${Resource.msg('global.requiredfield','locale',null)}</em></span></div>
		</legend>

		<div class="payment-method-options">

				<isloop items="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.options}" var="paymentMethodType">
	
					<iscomment>Ignore GIFT_CERTIFICATE method, GCs are handled separately before other payment methods.</iscomment>
					<isif condition="${paymentMethodType.value.equals(dw.order.PaymentInstrument.METHOD_GIFT_CERTIFICATE)}"><iscontinue/></isif>
	
					<isset name="radioID" value="${paymentMethodType.value}" scope="page"/>
					<div class="form-row is-${radioID}">
						<label for="is-${radioID}"><span class="offscreen"><isprint value="${Resource.msg(paymentMethodType.label,'forms',null)}"/></span></label>
	
						<input type="radio" class="input-radio" name="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.htmlName}" value="${paymentMethodType.htmlValue}"/>
					</div>
	
				</isloop>

		</div>

		<div class="form-row form-row-button">
			<button id="payment-select-go" name="${pdict.CurrentForms.billing.paymentSelect.htmlName}" type="submit" value="Go" class="simple-submit">Select</button>
		</div>

		<isif condition="${!empty(pdict.MissingPaymentProcessor)}">
			<div class="billing-error">${Resource.msg('billing.missingprocessorerror','checkout',null)}</div>
		</isif>
	
		<iscomment>
			Credit card block
			--------------------------------------------------------------
		</iscomment>
		<div class="payment-method <isif condition="${empty(pdict.selectedPaymentID) || pdict.selectedPaymentID=='CREDIT_CARD'}">payment-method-expanded</isif>" data-method="CREDIT_CARD">
			
			<iscomment>display select box with stored credit cards if customer is authenticated</iscomment>
			<isif condition="${pdict.CurrentCustomer.authenticated && !empty(pdict.ApplicableCreditCards)}">
				<iscomment>
					Invalid Credit Card Message - The Result of the Backend verification
				</iscomment>
				<isif condition="${pdict.InvalidCreditCardError != null && pdict.InvalidCreditCardError == true}">
					<isset name="InvalidCreditCardError" value="${pdict.InvalidCreditCardError}" scope="session" />
				</isif>
				<isif condition="${session.custom.InvalidCreditCardError != null && session.custom.InvalidCreditCardError == true}">
					<span class="credit-card-error">${Resource.msg('billing.creditcardinvalid','checkout',null)}</span>
					<isif condition="${pdict.RemoveCreditCardError != null && pdict.RemoveCreditCardError == true}">
						<isset name="InvalidCreditCardError" value="" scope="session" />
					</isif>
				</isif>
				
				<div class="form-row clearfix">
				<label>${Resource.msg('billing.creditcardlistselectcard', 'checkout', null)}</label>
					<div class="select-wrapper">
						<select name="${pdict.CurrentForms.billing.paymentMethods.creditCardList.htmlName}" id="creditCardList" class="input-select">
							<isloop items="${pdict.ApplicableCreditCards}" var="creditCardInstr">
								<option value="${creditCardInstr.UUID}" <isif condition="${creditCardInstr.custom.isDefault}">selected</isif>>(<isprint value="${creditCardInstr.creditCardType}"/>) <isprint value="${creditCardInstr.maskedCreditCardNumber}"/>
									<isif condition="${creditCardInstr.creditCardExpirationMonth != null && creditCardInstr.creditCardExpirationYear != null}">
									 - ${Resource.msg('billing.creditcardlistexp','checkout',null)} <isprint value="${creditCardInstr.creditCardExpirationMonth}" formatter="00" />/<isprint value="${creditCardInstr.creditCardExpirationYear}" formatter="00" encoding="off"/>
									</isif>
								</option>
							</isloop>
							<iscomment>option to create a new address</iscomment>
							<option value="newCard">${Resource.msg('billing.createnewcard','checkout',null)}</option>

						</select>
					</div>

				</div>

				<div class="form-row form-row-button">
					<button id="credit-card-select-go" name="${pdict.CurrentForms.billing.creditCardSelect.htmlName}" type="submit" value="Go" class="simple-submit">Select</button>
				</div>

				<div class="chosen-cc-display clearfix">
					<div>${Resource.msg('billing.ccowner','checkout',null)} <span id="chosen-cc-name"></span> </div>
					<div>${Resource.msg('billing.cctype','checkout',null)} <span id="chosen-cc-type"></span></div>
					<div>${Resource.msg('billing.ccnumber','checkout',null)} <span id="chosen-cc-maskedNo"></span></div>
					<span id="chosen-cc-exp">${Resource.msg('billing.creditcardlistexpdate','checkout',null)} <span id="chosen-cc-expM"></span>/<span id="chosen-cc-expY"></span></span>
				</div>

				<iscomment>
					<isloop items="${pdict.ApplicableCreditCards}" var="creditCardInstr">
						<a href="${URLUtils.https('COBilling-UpdateCreditCardSelection', 'creditCardUUID', creditCardInstr.UUID)}">
							(<isprint value="${creditCardInstr.creditCardType}"/>)
							<isprint value="${creditCardInstr.maskedCreditCardNumber}"/>
							- ${Resource.msg('billing.creditcardlistexp','checkout',null)}
							<isprint value="${creditCardInstr.creditCardExpirationMonth}" formatter="00" />
							/<isprint value="${creditCardInstr.creditCardExpirationYear}" formatter="0000" />
						</a>
					</isloop>
				</iscomment>

			</isif>

			<div class="cc-form expanded">
				<isinputfield formfield="${pdict.CurrentForms.billing.paymentMethods.creditCard.owner}" type="input" attribute1="autocomplete" value1="off" xhtmlclass="owner"/>
	
				<isinputfield formfield="${pdict.CurrentForms.billing.paymentMethods.creditCard.type}" type="select"/>
	
				<isinputfield formfield="${pdict.CurrentForms.billing.paymentMethods.creditCard.number}" type="input" attribute1="autocomplete" value1="off" xhtmlclass="creditCardNumber"/>
				
	
				<div class="form-row exp-row">
					<label class="form-label-text exp">${Resource.msg('billing.creditcardlistexpdate','checkout',null)}</span><span class="required-indicator"></label>
					
					<div class="select-month">
						<isinputfield formfield="${pdict.CurrentForms.billing.paymentMethods.creditCard.month}" type="select" rowclass="month label-removed"/>
					</div>
		
					<div class="select-year">
						<isinputfield formfield="${pdict.CurrentForms.billing.paymentMethods.creditCard.year}" type="select"  rowclass="year label-removed"/>
					</div>
				</div>
			</div>
		
			<iscomment>the cvn input is always visible when Credit Card is selected</iscomment>
			<div class="input-cvn">

				<div class="form-field-tooltip <isif condition="${pdict.CurrentForms.billing.paymentMethods.creditCard.cvn.error == null}">cvn-tip <iselse/> cvn-tip-error </isif>">
					<a href="${URLUtils.url('Page-Show', 'cid', 'checkout-security-code')}" class="tooltip">
						<span class="help-icon"></span>
						<div class="tooltip-content" data-layout="small tooltip-cvn">
							<isslot id="checkout-security-code" description="Checkout Billing CVN Tooltip" context="global">
						</div>
					</a>
				</div>

			
				<isinputfield formfield="${pdict.CurrentForms.billing.paymentMethods.creditCard.cvn}" type="input" rowclass="cvn" attribute1="autocomplete" value1="off" attribute2="minlength" value2="3" xhtmlclass="cvn"/>
			</div>
			<isif condition="${pdict.CurrentCustomer.authenticated}">
				<div class="saveCard">
					<isinputfield formfield="${pdict.CurrentForms.billing.paymentMethods.creditCard.saveCard}" type="checkbox" rowclass="label-inline form-indent"/>
				</div>
				<div class="hiddenDefault">
					<isinputfield formfield="${pdict.CurrentForms.billing.paymentMethods.creditCard.isDefault}" type="checkbox"/>
				</div>
			</isif>

			<iscomment>Only plug in the tokenization if the selected method of payment is a credit card
						and it is not a saved credit card that had been tokenized.
			</iscomment>
			<isif condition="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.value == 'CREDIT_CARD' && dw.system.Site.getCurrent().getCustomPreferenceValue('firstdataEnabled')}">
				<div id="fdp_inputs">
					<isinclude template="creditcard/firstdatapayeezyinputs"/>
					<isinclude template="creditcard/firstdatapayeezybilling"/>
				</div>
			</isif>
			<input type="hidden" id="cardToken" name="cardToken" value="" />
			<input type="hidden" id="cardBin" name="cardBin" value="" />
		</div>

		<iscomment>
			Bill me later
			--------------------------------------------------------------
		</iscomment>

		<div class="payment-method <isif condition="${!empty(pdict.selectedPaymentID) && pdict.selectedPaymentID=='BML'}">payment-method-expanded</isif>" data-method="BML">

			<p class="form-caption">${Resource.msg('billing.bmlhelp','checkout',null)}</p>

		    <span class="form-label-text"><span class="required-indicator">&#42;</span>Date of Birth:</span>
			<isinputfield formfield="${pdict.CurrentForms.billing.paymentMethods.bml.year}" type="select" rowclass="year label-removed"/>
			<isinputfield formfield="${pdict.CurrentForms.billing.paymentMethods.bml.month}" type="select" rowclass="month label-removed"/>
			<isinputfield formfield="${pdict.CurrentForms.billing.paymentMethods.bml.day}" type="select" rowclass="day label-removed"/>

			<isinputfield formfield="${pdict.CurrentForms.billing.paymentMethods.bml.ssn}" type="input" xhtmlclass="ssn"/>

			<div class="bml-terms-and-conditions form-caption">
				<isslot id="bml-tc" description="bml-tc" context="global">
			</div>

			<div class="form-row form-caption">
				<isinputfield formfield="${pdict.CurrentForms.billing.paymentMethods.bml.termsandconditions}" type="checkbox" rowclass="label-inline"/>
			</div>

		</div>

		<iscomment>
			Custom processor
			--------------------------------------------------------------
		</iscomment>

		<div class="payment-method <isif condition="${!empty(pdict.selectedPaymentID) && pdict.selectedPaymentID=='PayPal'}">payment-method-expanded</isif>" data-method="Custom">
			<iscomment>
				<div class="pay-pal-form-row">
					<isinclude template="includes/paymentmethodsinclude" />
					${Resource.msg('billing.custompaymentmethod','checkout',null)}
				</div>
			</iscomment>
		</div>
</fieldset>

<iselse>
<iscomment>
Zero Balance after applying gift card
--------------------------------------------------------------
</iscomment>

	<div class="gift-cert-used-no-balance">
		<isif condition="${pdict.gcPITotal > 0}">
			${Resource.msg('billing.giftcertnomethod','checkout',null)}
		<iselse/>
			${Resource.msg('billing.zerobalance','checkout',null)}
		</isif>
		<input type="hidden" name="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.htmlName}" value="${dw.order.PaymentInstrument.METHOD_GIFT_CERTIFICATE}" />
		<input type="hidden" id="noPaymentNeeded" name="noPaymentNeeded" value="true" />
	</div>

</isif>
<div class="billing-error"></div>