var PayeezyResponse=function(){
	/* the form and form fields can be set by the page
	 * where the tokenization occurs. There may be more than one.
	 * Card number field should NEVER be empty!
	 * Form should ALWAYS be submitted except for 400 Bad Request errors
	 * 400 Errors bring back CC validation through Payeezy 
	*/
	var $form = undefined;
	var $cardNumber = undefined;
	var $cardToken = undefined;
	
	return {
		responseHandler:function(status, response) {
			if (status != 201) {
				//bring out the bad response errors
				if (status == 400 && response.error) {
					var errorMessages = response.error.messages;
					var allErrors = '';
					for (i=0; i<errorMessages.length;i++) {
						allErrors = allErrors + errorMessages[i].description;
					}
					$form.find('.billing-error').text(allErrors);
					$cardNumber.val("");
				}
				else{
					$form.get(0).submit();
				}
			} else {
				// success 
				var token = response.token.value;
				// replace form cc number with token
				if (token.length != 0) {
					$cardNumber.val('************' + token.substr(token.length - 4));
				}
				// store the token on the PaymentInstrument
				$cardToken.val(token);
				$form.get(0).submit();
			}	
		},
		setCardNumberInput:function(cardNumberInput){
			$cardNumber = cardNumberInput;
		},
		setForm:function(pageForm){
			$form = pageForm;
		},
		setTokenInput:function(tokenInput){
			$cardToken = tokenInput;
		},
	}
}();