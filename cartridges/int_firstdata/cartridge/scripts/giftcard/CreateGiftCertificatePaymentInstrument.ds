/**
 * Creates a gift certificate payment instrument from the given gift certificate ID
 * for the given basket. The script attempts to redeem the current balance of
 * the gift certificate. If the current balance exceeds the order total, this amount
 * is redeemed and the balance is lowered.
 *
 *  @input Basket : dw.order.Basket The basket.
 *  @input GiftCertificateID : String The Gift Certificate ID.
 *  @input GiftCertificatePin : String The Gift Certificate PIN.
 *  @output GiftCertStatus : dw.system.Status The status of the gift certificate redemption.
 *  @output PaymentInstrument : dw.order.PaymentInstrument The created Payment instrument.
 *  @output Balance : Number The balance of the GiftCertificate
 *
 */

importPackage( dw.system );
importPackage( dw.order );
importPackage( dw.value );
importPackage( dw.util );
importScript( "giftcard/GiftCardOperationsGGe4.ds" );

function execute( pdict : PipelineDictionary ) : Number
{
    var basket : Basket = pdict.Basket;
	var giftCertID : String = pdict.GiftCertificateID;
	var giftCertPin : String = pdict.GiftCertificatePin;

	// fetch the gift certificate balance
	var gc : Object = GiftCardOperations.checkBalance(giftCertID, giftCertPin);
	
	if( gc == null || !empty(gc.errorMessage))
	{
		
		pdict.GiftCertStatus = new Status(Status.ERROR, gc.errorMessage );
		return PIPELET_ERROR;	
	}

	// make sure it has not been fully redeemed
	if( new Number(gc.currentBalance) <= 0 )
	{
		pdict.GiftCertStatus = new Status(Status.ERROR, GiftCertificateStatusCodes.GIFTCERTIFICATE_INSUFFICIENT_BALANCE);
		return PIPELET_ERROR;
	}

	// remove any duplicates
	removeDuplicates(basket, giftCertID);

	// fetch the order total and set balance
	var orderTotal : Money = basket.getTotalGrossPrice();
	var balance : Money = new Money(new Number(gc.currentBalance) , orderTotal.currencyCode);

	// assume to redeem the remaining balance
	var amountToRedeem : Money = balance;

	// because there may be multiple gift certificates, we adjust the amount being applied to the current 
	// gift certificate based on the order total minus the aggregate amount of the current gift certificates.
	var amountToRedeem = calculateAmount(amountToRedeem, orderTotal, basket);

	// create a payment instrument from this gift certificate
	var paymentInstr : PaymentInstrument = basket.createGiftCertificatePaymentInstrument(giftCertID, amountToRedeem);
	//paymentInstr.getPaymentTransaction().setTransactionID(giftCertPin);
	pdict.PaymentInstrument = paymentInstr;

	// put status OK
	pdict.GiftCertStatus = new Status(Status.OK);

	//setup for creating a GC if one does not exist, this is for correct export
	pdict.Balance = new Number(gc.currentBalance);

    return PIPELET_NEXT;
}

/**
 * Calculates the amount to redeem for this gift certificate by subtracting 
 * the amount of all of other gift certificates from the order total.
 */
function calculateAmount(amountToRedeem : Money, orderTotal : Money, basket : Basket) 
{
	// the total redemption amount of all gift certificates for the basket
	var giftCertTotal : Money = new Money(0.0, Site.getCurrent().getDefaultCurrency());

	// iterate over the list of gift certificate payment instruments 
	// and update the total redemption amount
	var gcPaymentInstrs : Collection = basket.getGiftCertificatePaymentInstruments();
	var iter : Iterator = gcPaymentInstrs.iterator();
	var orderPI : OrderPaymentInstrument = null;

	while( iter.hasNext() )
	{
		orderPI = iter.next();
		giftCertTotal = giftCertTotal.add( orderPI.getPaymentTransaction().getAmount() );
	}

	// calculate the remaining order balance
	// this is the remaining open order total which has to be paid
	var orderBalance : Money = orderTotal.subtract( giftCertTotal );

	// the redemption amount exceeds the order balance
	// return the order balance as maximum redemption amount
	if( orderBalance < amountToRedeem ) 
	{
		// return the remaining order balance
		return orderBalance;
	}

	// just return the redemption amount in case it is lower
	// or equals the order balance
	return amountToRedeem;
}

/** 
 * Determines if the basket already contains a gift certificate payment 
 * instrument with the given gift certificate ID and removes these existing 
 * payment instrument from the basket.
 */
function removeDuplicates(basket : Basket, giftCertID : Object)
{
	// iterate over the list of payment instruments to check 
	// if the gift certificate is already being used as payment instrument
	var gcPaymentInstrs : Collection = basket.getGiftCertificatePaymentInstruments(giftCertID);
	var iter : Iterator = gcPaymentInstrs.iterator();
	var existingPI : OrderPaymentInstrument = null;
	
	// remove found gift certificates, since we don't want duplicates
	while( iter.hasNext() )
	{
		existingPI = iter.next();
		basket.removePaymentInstrument( existingPI );
	}
}
