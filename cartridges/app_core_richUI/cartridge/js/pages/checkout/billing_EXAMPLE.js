'use strict';

var ajax = require('../../ajax'),
	customInputs = require('../../custominputs'),
	formPrepare = require('./formPrepare'),
	progress = require('../../progress'),
	giftcard = require('../../giftcard'),
	tooltip = require('../../tooltip'),
	stickyheader = require('../../stickyheader'),
	util = require('../../util'),
	billingExt = require('./billing-ext'),
	FDIncludes = $('#fdp_inputs').html(), //cache the tokenization includes so they can removed and added to the DOM as needed
	selectedCC = ($('#creditCardList').length > 0 ? $('#creditCardList').val() : 'newCard'), // cache chosen saved CC
	selectedCCType = ($('id$="creditCard_type"').length > 0 ? $('id$="creditCard_type"').val() : ''),
	ccv = require('./creditCardValidator');
/**
 * @function
 * @description Fills the Credit Card form with the passed data-parameter and clears the former cvn input
 * @param {Object} data The Credit Card data (holder, type, masked number, expiration month/year)
 */
function setCCFields(data) {
	var $creditCard = $('[data-method="CREDIT_CARD"]');
	$creditCard.find('input[name$="creditCard_owner"]').val(data.holder).trigger('change');
	$creditCard.find('select[name$="_type"]').val(data.type).trigger('change');
	if (data.cardToken === null) {
		$creditCard.find('input[name$="_number"]').val(data.maskedNumber).trigger('change');
	} else {
		$creditCard.find('input[name$="_number"]').val(data.cardToken).trigger('change');
	}
	if (data.type === 'Brand') {
		$creditCard.find('[name$="_month"]').val('').trigger('change');
		$creditCard.find('[name$="_year"]').val('').trigger('change');
	} else {
		$creditCard.find('[name$="_month"]').val(data.expirationMonth).trigger('change');
		$creditCard.find('[name$="_year"]').val(data.expirationYear).trigger('change');
	}

	$creditCard.find('input[name$="_cvn"]').val('').trigger('change');
	if (data.cardToken) {
		$creditCard.find('input[name$="cardToken"]').val(data.cardToken).trigger('change');
	} else {
		$creditCard.find('input[name$="cardToken"]').val('');
	}
	if (data.cardBin) {
		$creditCard.find('input[name$="cardBin"]').val(data.cardBin).trigger('change');
	} else {
		$creditCard.find('input[name$="cardBin"]').val('');
	}
	//set the hidden input for isDefault
	$creditCard.find('input[name$="isDefault"]').prop('checked', (data.isDefault === null ? false : data.isDefault)).val(data.isDefault === null ? false : data.isDefault);

	//populate the text display
	$('#chosen-cc-name').html(data.holder);
	$('#chosen-cc-type').html(data.type);
	$('#chosen-cc-maskedNo').html(data.maskedNumber);
	$('#chosen-cc-expM').html(data.expirationMonth);
	$('#chosen-cc-expY').html(data.expirationYear);

	//Amex needs its cvn to be validated to 4-digits
	if (data.type === 'Amex' || data.type === 'American Express') {
		$creditCard.find('input[name$="_cvn"]').attr('minlength', '4').attr('maxlength', '4');
	} else {
		$creditCard.find('input[name$="_cvn"]').attr('minlength', '3').attr('maxlength', '3');
	}
	//save a reference to the FD inputs
	if (data.type === 'Brand') {
		$('#chosen-cc-exp').hide();
		$('#fdp_inputs').html('');
		setBrandCardBin();
	} else {
		$('#chosen-cc-exp').show();
		//$('#fdp_inputs').html(FDIncludes);
		tooltip.init();
	}
}

/**
 * @function
 * @description Updates the credit card form with the attributes of a given card
 * @param {String} cardID the credit card ID of a given card
 */
function populateCreditCardForm(cardID) {
	var cardData = {
			holder: '',
			type: selectedCCType,
			maskedNumber: '',
			expirationMonth: '',
			expirationYear: '',
			cardToken: '',
			cardBin: '',
			isDefault: false
	};
	if (cardID === 'newCard' || cardID === null) {
		setCCFields(cardData);
		$('.cc-form').addClass('expanded');
		$('.chosen-cc-display').hide();
		$('.saveCard').show();
		if (cardData.type === 'Brand') {
			$('#fdp_inputs').html('');
			setBrandCardBin();
		} else {
			$('#fdp_inputs').html(FDIncludes);
		}
		tooltip.init();
		return;
	}

	// load card details
	var url = util.appendParamToURL(Urls.billingSelectCC, 'creditCardUUID', cardID);
	ajax.getJson({
		url: url,
		callback: function (data) {
			if (!data) {
				//window.alert(Resources.CC_LOAD_ERROR);
				setCCFields(cardData);
				$('.cc-form').addClass('expanded');
				$('.chosen-cc-display').hide();
				//ensure firstdata inputs available
				$('#fdp_inputs').html(FDIncludes);
				$('.saveCard').show();
				tooltip.init();
				return false;
			}
			$('.cc-form').removeClass('expanded');
			setCCFields(data);
			$('.chosen-cc-display').show();
			//ensure firstdata inputs are gone
			$('#fdp_inputs').html('');
			setBrandCardBin();
			$('.saveCard').hide();
			tooltip.init();
		}
	});
}
/**
 * @function
 * @description handles client-side credit card validation results
 */
function handleValidity(isValid) {
	var continueButton = $('button[name$="billing_save"]');
	if (isValid) {
		//clear errors, enable continue button
		if ($('#ccvalidation-error').length > 0) { $('#ccvalidation-error').remove(); }
	} else {
		//add errors, disable continue button
		$('#ccvalidation-error').remove();
		$('input[name$="creditCard_number"]').after('<span id="ccvalidation-error" class="error"> Invalid card number.</span>');
		continueButton.attr('disabled', 'disabled');
	}
}

/**
 * @function
 * @description initializes the credit card section in the payment methods section
 */
function initPaymentMethodSection() {
	var $selectPaymentMethod = $('.payment-method-options');
	//init saveCard checkbox
	customInputs.checkbox('.saveCard .input-checkbox');

	// init change event on list
	$('#creditCardList').on('change', function () {
		selectedCC = $(this).val();
		var cardUUID = $(this).val();
		if (!cardUUID || cardUUID === null || cardUUID === '') {return;}
		populateCreditCardForm(cardUUID);

		// remove server side error
		$('.required.error').removeClass('error');
		$('.error-message').remove();
		$('.credit-card-error').remove();
		if ($('.billing-error').length > 0) { $('.billing-error').remove(); }
	});

	// init change of the form on load for Brand Card Type
	var detachedExpHtml = $('.exp-row').html();
	var detachedCvnHtml = $('.input-cvn').html();
	if ($('select[name$="creditCard_type"]').val() === 'Brand') {
		$('.exp-row').html('');
		$('.input-cvn').html('');
		$('#fdp_inputs').html('');
		setBrandCardBin();
	} else {
		$('.exp-row').html(detachedExpHtml);
		$('.input-cvn').html(detachedCvnHtml);
		$('#fdp_inputs').html(FDIncludes);
		tooltip.init();
	}
	if ($('select[name$="creditCard_type"]').val() === 'Amex' || $('select[name$="creditCard_type"]').val() === 'American Express') {
		$('input[name$="_cvn"]').attr('minlength', '4').attr('maxlength', '4');
	} else {
		$('input[name$="_cvn"]').attr('minlength', '3').attr('maxlength', '3');
	}

	$('select[name$="creditCard_type"]').on('change', function () {
		selectedCCType = $(this).val();
		if ($(this).val() === 'Brand') {
			$('.exp-row').html('');
			$('.input-cvn').html('');
			$('#fdp_inputs').html('');
			setBrandCardBin();
		} else {
			$('.exp-row').html(detachedExpHtml);
			$('.input-cvn').html(detachedCvnHtml);
			$('#fdp_inputs').html(FDIncludes);
			tooltip.init();
		}
		//Amex needs its cvn to be validated to 4-digits
		if ($(this).val() === 'Amex' || $(this).val() === 'American Express') {
			$('input[name$="_cvn"]').attr('minlength', '4').attr('maxlength', '4');
		} else {
			$('input[name$="_cvn"]').attr('minlength', '3').attr('maxlength', '3');
		}
		//clear form data to avoid mismatch number and type #575261
		var CCNo = $('[data-method="CREDIT_CARD"]').find('input[name$="_number"]').val();
		if (CCNo.length > 0) {
			$('[data-method="CREDIT_CARD"]').find('input[name$="_number"]').val('');
			$('[data-method="CREDIT_CARD"]').find('input[name$="_cvn"]').val('');
			if ($('#ccvalidation-error').length > 0) { $('#ccvalidation-error').remove(); }
		}
		//reinit the form fields for validation
		if ($('.cc-form').length > 0) {
			//re-init forms so payment methods section validates
			formPrepare.init({
				formSelector: 'form[id$="billing"]',
				continueSelector: '[name$="billing_save"]'
			});
		}
	});
	// clean server side errors on focus
	$('input[name$="creditCard_number"]').on('focus', function () {
		$('.required.error').removeClass('error');
		$('.error-message').remove();
		if ($('.billing-error').length > 0) { $('.billing-error').remove(); }
		if ($('#ccvalidation-error').length > 0) { $('#ccvalidation-error').remove(); }
	//add special client-side credit card validation
	}).on('blur', function () {
			var type = $('select[name$="creditCard_type"]').val();
			var number = $('input[name$="creditCard_number"]').val();
			var valid = ccv.creditCardValidator.validate(number, type);
			if (!valid) {
				if (number === '' || number === null) { return; }
				handleValidity(valid);
			}
		});
	//add click event to radio buttons
	$selectPaymentMethod.on('click', 'input[type="radio"]', function () {
		$(this).closest('.payment-method-options').find('.show-radio').removeClass('show-radio');
		$(this).addClass('show-radio');
		updatePaymentMethod($(this).val());
	});
	if ($('#creditCardList').length > 0) {
		//if cc list exists select first option in drop down and fill in form
		$('#creditCardList').val(selectedCC);
		populateCreditCardForm(selectedCC !== null ? selectedCC : $('#creditCardList').first('option').val());
		//remove firstdata inputs since it is a saved card
		$('#fdp_inputs').html('');
		setBrandCardBin();
	} else {
		populateCreditCardForm(selectedCC);
	}
	// select credit card radio
	var ccRadio = $('.payment-method-options input[value="CREDIT_CARD"]');
	ccRadio.prop('checked', true).addClass('show-radio');

	//reinit the form fields for validation
	if ($('.cc-form').length > 0) {
		//re-init forms so payment methods section validates
		formPrepare.init({
			formSelector: 'form[id$="billing"]',
			continueSelector: '[name$="billing_save"]'
		});
	}
}

/**
 * @function
 * @description Changes the payment method form depending on the passed paymentMethodID
 * @param {String} paymentMethodID the ID of the payment method, to which the payment method form should be changed to
 */
function updatePaymentMethod(paymentMethodID) {
	var $paymentMethods = $('.payment-method');
	$paymentMethods.removeClass('payment-method-expanded');
	var continueButton = $('button[name$="billing_save"] span');

	if (paymentMethodID === 'PayPal') {
		continueButton.html('Continue to PayPal').parent().removeAttr('disabled');
		if ($('.billing-error').length > 0) { $('.billing-error').remove(); }
		//make sure all hidden inputs are removed
		$('[data-method="CREDIT_CARD"]').remove();
		//suppress billing address form validation
		$('#dwfrm_billing').addClass('suppress');
		$('#dwfrm_billing .required').find(':input').removeClass('required').addClass('suppress');
		//reinit for for class to take affect
		$('#dwfrm_billing').validate().form();

	} else {
		//allow for billing address form validation
		$('#dwfrm_billing').removeClass('suppress');
		$('#dwfrm_billing .required').find(':input').addClass('required').removeClass('suppress');
		//reinit form validation
		// remove server side error
		$('.required.error').removeClass('error');
		$('.error-message').remove();
		if ($('.billing-error').length > 0) { $('.billing-error').remove(); }
		updatePaymentMethods();
		continueButton.html('Continue');
	}

	var $selectedPaymentMethod = $paymentMethods.filter('[data-method="' + paymentMethodID + '"]');
	if ($selectedPaymentMethod.length === 0) {
		$selectedPaymentMethod = $('[data-method="Custom"]');
	}
	$selectedPaymentMethod.addClass('payment-method-expanded');

	// ensure radio of payment method is checked
	$('input[name$="_selectedPaymentMethodID"]').removeAttr('checked');
	$('input[value=' + paymentMethodID + ']').attr('checked', 'checked');
}

/**
 * @function
 * @description updates the payment methods section
 */
function updatePaymentMethods() {
	var $methods = $('.payment-methods');
	// load the updated area
	$methods.load(Urls.updatePaymentMethodSection, function () {
		initPaymentMethodSection();
		customInputs.IEnormalize($('.input-radio'));
	});
}

/**
 * @function
 * @description updates the order summary based on a possibly recalculated basket after a shipping promotion has been applied
 */
function updateSummary() {
	var $summary = $('#secondary.summary');
	// indicate progress
	progress.show($summary);

	// load the updated summary area
	$summary.load(Urls.summaryRefreshURL, function () {
		// hide edit shipping method link
		$summary.fadeIn('fast');
		$summary.find('.checkout-mini-cart .minishipment .header a').hide();
		$summary.find('.order-totals-table .order-shipping .label a').hide();
		tooltip.init();

		if (billingExt && billingExt.init) {
			billingExt.init();
		}
	});
}

/**
 * @function
 * @description updates the Gift Certificates Applied list in the accordion
 */
function updateGiftCertApplied() {

	var appliedGC = $('.gc-accordion-inner .redemption');

	// load the updated summary area
	appliedGC.load(Urls.updateGiftCertApplied, function () {
		//make sure accordion is open
		$('.gc-accordion-wrapper').addClass('expanded');
		//needed to init remove button on refreshed promo container
		initGCRemoveEvent();
		//clear the text input
		$('input[name$="giftCertCode"]').val('');
		$('.checkout-billing').find('.giftcert-error').html('');
		//refresh the payment methods section
		updatePaymentMethods();

	});
}

/**
 * @function
 * @description inits click evens on promo remove
 */
function initGCRemoveEvent() {

	$('.remove-gc').on('click', function (e) {
		e.preventDefault();
		var code = $(this).prev('.success').html();
		var $error = $('.checkout-billing').find('.giftcert-error');
		removeGiftCertificate(code);
		$error.html('');
	});
}
/**
 * @function
 * @description updates the Promos Applied list after a promotion has been applied
 */
function updateCouponsApplied() {

	var appliedPromos = $('.promo-accordion-inner .redemption');

	// load the updated summary area
	appliedPromos.load(Urls.updatePromosApplied, function () {
		//make sure accordion is open
		$('.promo-accordion-wrapper').addClass('expanded');
		//needed to init remove button on refreshed promo container
		initCouponRemoveEvent();
		//clear the text input
		$('input[name$="couponCode"]').val('');
		// Update Round up amount if it exists
		var roundUpCheckbox = $('.roundup-checkbox');
		if (roundUpCheckbox !== null && roundUpCheckbox.prop('checked') === true) {
			$('.htRoundUp').trigger('click');
			setTimeout(function () {
				$('.htRoundUp').trigger('click');
			}, 2000);
		}
	});
}

/**
 * @function
 * @description inits click evens on promo remove
 */
function initCouponRemoveEvent() {

	$('.remove-coupon').on('click', function (e) {
		e.preventDefault();
		var code = $(this).prev('.success').html();
		var $error = $('.checkout-billing').find('.coupon-error');
		var url = util.appendParamsToUrl(Urls.removeCoupon, {couponCode: code, format: 'ajax'});
		$.getJSON(url, function (data) {
			var fail = false;
			var msg = '';
			if (!data) {
				msg = Resources.BAD_RESPONSE;
				fail = true;
			} else if (!data.success) {
				msg = data.message.split('<').join('&lt;').split('>').join('&gt;');
				fail = true;
			}
			if (fail) {
				$error.html(msg);
				return;
			}
			if (data.success) {
				//update applied promotions
				updateCouponsApplied();
				//update cart summary
				updateSummary();
			}
		});
	});
}

/**
 * @function
 * @description adds the loyalty reward
 */
function addLoyaltyReward(selectedReward) {

	var $reward = selectedReward;
	var rewardData = selectedReward.data('loyalty-reward-detail'),
	rewardID = rewardData.rewardID,
	rewardName = rewardData.rewardName,
	rewardType = rewardData.rewardType,
	rewardValue = rewardData.rewardValue;

	// Check if Gift Certificate is applied and remove it if so, because after applying rewards the ammount will be changed
	var code = $('.remove-gc').prev('.success').html();
	if (code !== null) {
		removeGiftCertificate(code);
	}

	var url = util.appendParamsToUrl(Urls.addLoyaltyReward, {rewardID: rewardID, rewardName: rewardName, rewardType: rewardType, rewardValue: rewardValue, format: 'ajax'});
	$.getJSON(url, function (data) {
		var fail = false;
		var msg = '';
		if (!data) {
			msg = Resources.BAD_RESPONSE;
			fail = true;
		} else if (!data.success) {
			fail = true;
		}
		if (fail) {
			return;
		} else {
			// ensure checkbox of the reward is checked
			if (data.rewardApplied === true) {
				$reward.attr('checked', 'checked').closest('.custom-checkbox').addClass('show-check').children('.reward-applied').css('display', 'inline-block');
				$reward.data('applied', false);
				// Apply Gift Certificate if was applied before removing
				if (code !== null) {
					redeemGiftCertificate(code);
				}
				updateSummary();
				updatePaymentMethods();
				if (data.basketMerchandizeTotalNetPrice === 0) {
					var continueButton = $('button[name$="billing_save"] span');
					continueButton.html('Continue').parent().removeAttr('disabled');
					disableRewards();
				}
			} else if (data.rewardApplied === false || data.rewardApplied === null) {
				updateSummary();
				//if no more rewards can be applied, uncheck this one and disable all unchecked
				$reward.attr('checked', '').closest('.custom-checkbox').removeClass('show-check').addClass('disabled').children('.reward-applied').css('display', 'none');
				disableRewards();
			}
		}
	});
}

/**
 * @function
 * @description removes the loyalty reward
 */
function removeLoyaltyReward(selectedReward) {
	var $reward = selectedReward;
	var rewardData = selectedReward.data('loyalty-reward-detail'),
	rewardID = rewardData.rewardID;

	// Check if Gift Certificate is applied and remove it if so, because after applying rewards the ammount will be changed
	var code = $('.remove-gc').prev('.success').html();
	if (code !== null) {
		removeGiftCertificate(code);
	}

	var url = util.appendParamsToUrl(Urls.removeLoyaltyReward, {rewardID: rewardID, format: 'ajax'});
	var msg = '';
	$.getJSON(url, function (data) {
		var fail = false;
		if (!data) {
			msg = Resources.BAD_RESPONSE;
			fail = true;
		} else if (!data.success) {
			fail = true;
		}
		if (fail) {
			return;
		} else {
			// uncheck checkbox and remove applied indicator
			$reward.prop('checked', false);
			$reward.removeAttr('checked').closest('.custom-checkbox').removeClass('show-check').children('.reward-applied').hide();
			$reward.data('applied', false);
			// Apply Gift Certificate if was applied before removing
			if (code !== null) {
				redeemGiftCertificate(code);
			}
			updateSummary();
			//check if this releases disabled rewards
			if ($reward.parent().siblings().hasClass('disabled')) {
				enableRewards();
			}
			updatePaymentMethods();
		}
	});
}

/**
 * @function
 * @description resets Shipping Address after toggling radios
 */
function resetShippingAddress() {
	$('.sameAsShippingDisplay').show();
	//ensure form, required dialog indicator and address are hidden
	$('.checkout-billing .addressForm').addClass('NoForm');
	$('.billingAddressDisplay').removeClass('showAddress');

	var shippingAddy = $('.shippingAddress').data('address');
	util.fillAddressFields(shippingAddy, $('.address'));
	//this is needed to set the cc form and preserve its type
	populateCreditCardForm(selectedCC);
}

/**
 * @function
 * @description updates custom check box style based on checked state
 */
function setRewardCheckboxState() {

	//set loyalty rewards that are checked
	var appliedRewards = $('.loyalty-rewards input[type="checkbox"]');
	for (var i = 0; i < appliedRewards.length; i++) {
		if (appliedRewards[i].checked) {
			$(appliedRewards[i]).closest('.custom-checkbox').addClass('show-check').children('.reward-applied').css('display', 'inline-block');
		}
	}
}

/**
 * @function
 * @description disables reward checkboxes when they can no longer be applied
 */
function disableRewards() {

	//add disabled to all unchecked checkboxes
	var appliedRewards = $('.loyalty-rewards input[type="checkbox"]');
	for (var i = 0; i < appliedRewards.length; i++) {
		if (appliedRewards[i].checked === false) {
			$(appliedRewards[i]).closest('.custom-checkbox').addClass('disabled');
		}
	}
}

/**
 * @function
 * @description enables reward checkboxes when they can no longer be applied
 */
function enableRewards() {

	//remove class disabled from custom-checkboxes
	var appliedRewards = $('.loyalty-rewards input[type="checkbox"]');
	for (var i = 0; i < appliedRewards.length; i++) {
		$(appliedRewards[i]).closest('.custom-checkbox').removeClass('disabled');
	}
}

/**
 * @function
 * @description on submit set Brand card Bin
 */
function setBrandCardBin() {
	$('#dwfrm_billing').off('submit:brand').on('submit:brand', function () {
		var $form = $(this);

		var $cardType = $('#dwfrm_billing_paymentMethods_creditCard_type');
		var $cardNumber = $('#dwfrm_billing_paymentMethods_creditCard_number');

		// Set card bin for Brand cards only
		var $cardBin = $('#cardBin');
		if ($cardType.val() === 'Brand' && $cardNumber.val() !== '' && $cardNumber.val().substr(0, 1) !== '*') {
			$cardBin.val($cardNumber.val().substr(0, 6));
		}
	});
}
/**
 * @function
 * @description redeems gift certificate
 */
function redeemGiftCertificate(code) {
	var $error = $('.giftcert-error');
	var url = util.appendParamsToUrl(Urls.redeemGiftCert, {giftCertCode: code, format: 'ajax'});
	$.getJSON(url, function (data) {
		var fail = false;
		var msg = '';
		if (!data) {
			msg = Resources.BAD_RESPONSE;
			fail = true;
		} else if (data.message && !data.success) {
			msg = data.message.split('<').join('&lt;').split('>').join('&gt;');
			if (msg === '') {
				msg = 'An error occurred. Please try again.';
			}
			fail = true;
		}
		if (data.success) {
			//update card applied list and payment methods
			updateGiftCertApplied();
			//update minicart
			updateSummary();
			//if the adjusted total is $0, enable continue button
			if (data.redemption.orderBalance <= 0) {
				$('.billing-submit button').removeAttr('disabled');
			}
		} else {
			if (data.redemptionErrorMsg.length > 0) {
				$error.html(data.redemptionErrorMsg);
			}
			$('input[name$="giftCertCode"]').addClass('error');
			return;
		}
		if (fail) {
			$error.html(msg);
			$('input[name$="giftCertCode"]').addClass('error');
			return;
		}
	});
}
/**
 * @function
 * @description removes gift certificate
 */
function removeGiftCertificate(code) {
	var url = util.appendParamsToUrl(Urls.removeGiftCert, {giftCertificateID: code, format: 'old'});
	$.getJSON(url, function (data) {
		var fail = false;
		var msg = '';
		if (!data) {
			msg = Resources.BAD_RESPONSE;
			fail = true;
		} else if (!data.success) {
			msg = data.message.split('<').join('&lt;').split('>').join('&gt;');
			fail = true;
		}
		if (fail) {
			$error.html(msg);
			return;
		}
		if (data.success) {
			//update applied promotions
			updateGiftCertApplied();
			//update cart summary
			updateSummary();

		}
	});
}
/**
 * @function
 * @description loads billing address, Gift Certificates, Coupon and Payment methods
 */
exports.init = function () {
	var $checkoutForm = $('.checkout-billing');
	var $addGiftCert = $('#add-giftcert');
	var $giftCertCode = $('input[name$="_giftCertCode"]');
	//var $giftCertPIN = $('input[name$="_giftCertPIN"]');
	var $addCoupon = $('#add-coupon');
	var $couponCode = $('input[name$="_couponCode"]');
	var $selectPaymentMethod = $('.payment-method-options');
	var selectedPaymentMethod = $selectPaymentMethod.find(':checked').val();
	var $loyaltyRewards = $('.loyalty-rewards');
	stickyheader.init();//reset header

	//billing.js is used for summary too
	//only init the forms if on actual billing page
	if ($('.checkout-billing').length > 0) {
		formPrepare.init({
			formSelector: 'form[id$="billing"]',
			continueSelector: '[name$="billing_save"]'
		});

		// default payment method to 'CREDIT_CARD'
		updatePaymentMethod((selectedPaymentMethod) ? selectedPaymentMethod : 'CREDIT_CARD');
		if (selectedPaymentMethod === 'CREDIT_CARD') {
			initPaymentMethodSection();
		}
	} else {
		//prevent double clicking of place order button in summary
		$('.submit-order').one('submit', function () {
			$('#main').find('button[type="submit"]').attr('disabled', 'disabled');
		});
	}

	//set state of promo and gift card accordion
	if ($('.giftcert-pi').length > 0) {
		$('.gc-accordion-wrapper').addClass('expanded');
	}
	//set state of promo and gift card accordion
	if ($('.promo-li').length > 0) {
		$('.promo-accordion-wrapper').addClass('expanded');
	}

	$('input[name$="giftCertCode"]').focus(function () {
		$('input[name$="giftCertCode"]').removeClass('error');
		$('.giftcert-error').html('');
	});

	$('input[name$="couponCode"]').focus(function () {
		$('input[name$="couponCode"]').removeClass('error');
		$('.coupon-error').html('');
	});

	updateSummary();
	initGCRemoveEvent();
	initCouponRemoveEvent();
	setRewardCheckboxState();

	//on init only make sure the new address drop down is set to create new and form is displayed if not using shipping address
	//if same as shipping is selected, show address under radio button
	if ($('#isSame').prop('checked') === true) {
		$('.sameAsShippingDisplay').show();
		//set saved address drop down to create new address but ensure form and address are hidden
		$('select[name$="billing_addressList"] option[value="newAddress"]').attr('selected', 'selected');
		$('.checkout-billing .addressForm').addClass('NoForm');
		$('.checkout-billing').find('.dialog-required').hide();
		$('.billingAddressDisplay').removeClass('showAddress');
	} else {
		$('.sameAsShippingDisplay').hide();
		if ($('select[name$="billing_addressList"]').length > 0) {
			$('select[name$="billing_addressList"] option[0]').attr('selected', 'selected');
			$('select[name$="billing_addressList"]').trigger('change');
		} else {
			$('.checkout-billing .addressForm').removeClass('NoForm');
			$('.checkout-billing').find('.dialog-required').show();
		}
	}
/*********************
 * Event handlers
 *********************/

	// add/ remove loyalty rewards
	$loyaltyRewards.on('click', '.custom-checkbox label', function () {
		if ($(this).parent('.custom-checkbox').hasClass('disabled')) {
			return;
		}
		var apply = $(this).siblings('.loyalty-reward').prop('checked') === false ? true : false;
		var selectedValue = $(this).siblings('.loyalty-reward').val(),
			$rewards = $('.loyalty-reward'),
			$selectedReward = $rewards.filter('[value=' + selectedValue + ']');

		if (apply === true) {
			addLoyaltyReward($selectedReward);
		} else {
			removeLoyaltyReward($selectedReward);
		}
	});

	//init click events on billing address radios
	$('.billing-address-options').on('click', '#isNew, #isSame', function () {
		//if same as shipping is selected, show address under radio button
		if ($('#isSame').prop('checked') === true) {
			$('.sameAsShippingDisplay').show();
			//ensure form, required dialog indicator and address are hidden
			$('.checkout-billing .addressForm').addClass('NoForm');
			$('.billingAddressDisplay').removeClass('showAddress');

			//guest users need to have the shipping address in the form if not creating new
			if ($('select[name$="billing_addressList"]').length === 0) {
				var shippingAddy = $('.shippingAddress').data('address');
				util.fillAddressFields(shippingAddy, $('.address'));
			}
		} else {
			if ($('select[name$="billing_addressList"]').length > 0) {
				$('.sameAsShippingDisplay').hide();
				$('select[name$="billing_addressList"]').trigger('change');
			} else {
				$('.sameAsShippingDisplay').hide();
				$('.checkout-billing .addressForm').removeClass('NoForm');
				var emptyAddy = {'key':'New', 'firstName':null, 'lastName':null, 'address1':null, 'address2':null, 'postalCode':null, 'city':null, 'stateCode':null, 'countryCode':'US', 'phone':null, 'type':'customer', 'displayValue':null};
				util.fillAddressFields(emptyAddy, $('.address'));
			}
		}

		//only show the dialog required indicator if the form is visible
		if ($('.NoForm').length === 1) {
			$('.checkout-billing').find('.dialog-required').hide();
		} else {
			$('.checkout-billing').find('.dialog-required').show();
		}

		//ensure cc form does not revert to init state; preserve selected CC
		if ($('.payment-method-options').find(':checked').val() === 'CREDIT_CARD') {
			populateCreditCardForm(selectedCC);
		}

	});

	//make radio button labels trigger radio selection
	$('.billing-address-options').on('click', '.shipHome', function () {
		$('#isSame').prop('checked', true).click();
	});
	$('.billing-address-options').on('click', '.shipNew', function () {
		$('#isNew').prop('checked', true).click();
	});

	//selecting same as shipping after unselecting it
	$('.checkout-billing').on('click', '#isSame', function () {
		resetShippingAddress();
	});

	$('#check-giftcert').on('click', function (e) {
		e.preventDefault();
		var $balance = $('.balance');
		if ($giftCertCode.length === 0 || $giftCertCode.val().length === 0) {
			var error = $balance.find('span.error');
			if (error.length === 0) {
				error = $('<span>').addClass('error').appendTo($balance);
			}
			error.html(Resources.GIFT_CERT_MISSING);
			return;
		}

		giftcard.checkBalance($giftCertCode.val(), function (data) {
			if (!data || !data.giftCertificate) {
				$balance.html(Resources.GIFT_CERT_INVALID).removeClass('success').addClass('error');
				return;
			}
			$balance.html(Resources.GIFT_CERT_BALANCE + ' ' + data.giftCertificate.balance).removeClass('error').addClass('success');
		});
	});

	$addGiftCert.on('click', function (e) {
		e.preventDefault();
		var code = $giftCertCode.val();
		var $error = $checkoutForm.find('.giftcert-error');
		if (code.length === 0) {
			$error.html(Resources.GIFT_CERT_MISSING);
			$('input[name$="giftCertCode"]').addClass('error');
			return;
		}
		redeemGiftCertificate(code);
	});

	$addCoupon.on('click', function (e) {
		e.preventDefault();
		var $error = $checkoutForm.find('.coupon-error'),
			code = $couponCode.val();
		if (code.length === 0) {
			$error.html(Resources.COUPON_CODE_MISSING);
			$('input[name$="couponCode"]').addClass('error');
			return;
		}

		var url = util.appendParamsToUrl(Urls.addCoupon, {couponCode: code, format: 'ajax'});
		$.getJSON(url, function (data) {
			var fail = false;
			var msg = '';
			if (!data) {
				msg = Resources.BAD_RESPONSE;
				fail = true;
			} else if (!data.success) {
				msg = data.message.split('<').join('&lt;').split('>').join('&gt;');
				fail = true;
			}
			if (fail) {
				$error.html(msg);
				$('input[name$="couponCode"]').addClass('error');
				return;
			}

			//basket check for displaying the payment section, if the adjusted total of the basket is 0 after applying the coupon
			//this will force a page refresh to display the coupon message based on a parameter message
			if (data.success && data.baskettotal === 0) {
				window.location.assign(Urls.billing);
			}
			//update applied promotions
			updateCouponsApplied();
			//update cart summary
			updateSummary();
		});
	});

	// trigger events on enter
	$couponCode.on('keydown', function (e) {
		if (e.which === 13) {
			e.preventDefault();
			$addCoupon.click();
		}
	});
	$giftCertCode.on('keydown', function (e) {
		if (e.which === 13) {
			e.preventDefault();
			$addGiftCert.click();
		}
	});

	$('.orderdiscounts').on('click', '.promo-accordion-wrapper h3, .gc-accordion-wrapper h3', function () {
		$(this).parent().toggleClass('expanded');
	});

};

exports.updatePaymentMethods = updatePaymentMethods;
exports.populateCreditCardForm = populateCreditCardForm;
exports.updateSummary = updateSummary;